# NZ_community_events

This project aims to provide output that can be used to represent community events (e.g., close and casual contacts) for the population of NZ.

## Folders

### code

The ` code` folder contains files:

- `COMPASS_contacts_plots.Rmd` - takes data from a contact survey ran by COMPASS to visualised binned distributions of contact numbers across demographic groups.

  output files from these scripts will be saved in `outputs`


### inputs

The `inputs` folder contains any input files required for analysis. 

### outputs

The `outputs` folder will be the target folder for any results saved by the `processing ` and `analysis` files.

## Authors and acknowledgment

Authors (order alphabetical): James Gilmour, Emily Harvey, Dion O'Neale, Steven Turnbull

This work was funded and supported by Te P\={u}naha Matatini and MBIE.

## License

Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
