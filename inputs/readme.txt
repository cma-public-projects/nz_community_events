Data provided by Barry Milne from the Compass synthetic population microdata.

Barry Milne <b.milne@auckland.ac.nz>


Hi Dion and all,
 
See attached.  I conducted basic crosstabulations between demographic measures and estimates of network size:
 
Self-reported estimates of daily contacts.
Self-reported estimates of daily contacts adjusted for the proportion of these that are face to face.
Self-reported frequency of going out.
 
They show similar patterns.  Age is a strongest demographic determinant of network size (older = smaller network).  Education (higher = larger network) and Occupation have impacts as well, but when I ran a model with age and the other factors, only age seemed to be important (model not included in the output).
 
I’m not sure we have any good data on church or community group participation but I’ll have a look.
 
Anyway, hope this helps.
 
All the best,
Barry
 
